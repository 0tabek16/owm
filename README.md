# OWM Proxy project

### Environment variables
```
NODE_ENV   - production
PORT       - port number for project
DB_URL     - mongo url
OWM_KEY    - Open weather map key
```

### To run the project
```bash
npm install
npm run start
```