const mongoose = require('mongoose')
const express = require('express')
const axios = require('axios')
const app = express()

const { NODE_ENV, PORT, DB_URL, OWM_KEY } = process.env

mongoose.connect( DB_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true
}).catch(err => {
  console.log(err)
})

const Connection = mongoose.model('connections', new mongoose.Schema({
  ip: {
    type: String,
    required: true
  },
  userAgent: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now()
  }
}))

app.get('/get', async (req, res) => {
  try {
    const weather = await axios.get(`https://api.openweathermap.org/data/2.5/forecast?appid=${OWM_KEY}&q=${req.query.q}`)
    res.json(weather.data)
  
    const newConnection = new Connection({
      ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
      userAgent: req.get('User-Agent')
    })
  
    await newConnection.save()
  } catch (err) {
    res.status(500).json({ err: 'There was an error!' })
  }
})

app.get('/all', async (req, res) => {
  try {
    const all = await Connection.find().sort({ date: -1 })
    res.json(all)
  } catch (err) {
    res.status(500).json({ err: 'Could not get!' })
  }
})

app.get('/info', (req, res) => {
  res.json({NODE_ENV})
})

app.listen(PORT, () => {
  console.log('Server running on :' + PORT)
})